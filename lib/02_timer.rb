class Timer

  def initialize
    @timer = 0
  end

  def seconds
    @timer
  end

  def seconds=(second)
    @timer = second
  end

  def time_string
    @hour = @timer/3600
    @minute = @timer/60 - @hour*60
    @second = @timer - @hour*60*60 - @minute*60
    @second = conversion(@second)
    @hour = conversion(@hour)
    @minute = conversion(@minute)
    @hour + ':' + @minute + ':' + @second
  end


  private

  def conversion(num)
    if num.to_s.length < 2
      '0' + num.to_s
    else
      num.to_s
    end
  end
end
