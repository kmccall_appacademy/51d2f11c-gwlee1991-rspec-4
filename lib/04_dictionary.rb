class Dictionary

  def initialize
  end

  def entries
    @dictionary_hash ||= Hash.new
    @dictionary_hash
  end

  def add (key_value)
    key_value = key_value.to_s
    key_value_pair = key_value.split(' => ')
    key = key_value_pair[0]
    value = key_value_pair[1]
    @dictionary_hash[key] = value
  end

  def keywords
    @dictionary_hash.keys
  end


end
