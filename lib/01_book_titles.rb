class Book
  attr_accessor :title
  def initialize
  end

  def title=(title)
    @title = cap(title)
  end

  private

  def cap(title)
    non_cap_words = ['a', 'an', 'and', 'in', 'the', 'of']
    words = title.split
    capitalized = words.map do |word|
      if non_cap_words.include?(word)
        word
      else
        word.capitalize
      end
    end
    first_word = capitalized[0].capitalize
    rest_words = ' ' + capitalized[1..-1].join(' ')
    if words.length > 1
      cap_title = first_word + rest_words
    else
      first_word
    end
  end
  
end
